package com.external;

import com.payoneer.jms.model.bean.internal.JobBean;
import com.payoneer.jms.model.bean.internal.JobExecuteResultBean;
import com.payoneer.jms.service.JobExecutorService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/*
 * This is an example of an external source to execute jobs with jms.
 * I am putting it inside the project for simplicity, other option would be to provide this dependency as a library.
 */
@Service
public class JobExecutorServiceImpl implements JobExecutorService {
    
    private final List<JobExecutorStrategy> jobExecutorStrategies;
    
    public JobExecutorServiceImpl(List<JobExecutorStrategy> jobExecutorStrategies) {
        this.jobExecutorStrategies = jobExecutorStrategies;
    }
    
    @Override
    public Optional<JobExecuteResultBean> executeJob(JobBean jobBean) {
        Optional<JobExecutorStrategy> jobExecutorStrategy = jobExecutorStrategies.stream().filter(jes -> jes.getType().equals(jobBean.getType())).findAny();
        if(jobExecutorStrategy.isEmpty()){
            return Optional.empty();
        }
        boolean result = jobExecutorStrategy.get().execute(jobBean.getName());
        return Optional.of(
                JobExecuteResultBean.builder().success(result).build()
        );
    }
}
