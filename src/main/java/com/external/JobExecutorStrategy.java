package com.external;

public interface JobExecutorStrategy {
    boolean execute(String name);
    
    String getType();
}
