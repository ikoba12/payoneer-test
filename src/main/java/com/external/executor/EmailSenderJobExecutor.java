package com.external.executor;

import com.external.JobExecutorStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EmailSenderJobExecutor implements JobExecutorStrategy {
    @Override
    public boolean execute(String name) {
        log.info("Pretending to send emails");
        return true;
    }
    
    @Override
    public String getType() {
        return "EmailSender";
    }
}
