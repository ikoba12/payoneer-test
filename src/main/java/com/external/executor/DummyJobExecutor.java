package com.external.executor;

import com.external.JobExecutorStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DummyJobExecutor implements JobExecutorStrategy {
    @Override
    public boolean execute(String name) {
        log.info("Not sure what to do I am a dummy executor");
        return false;
    }
    
    @Override
    public String getType() {
        return "Dummy";
    }
}
