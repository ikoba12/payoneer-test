package com.payoneer.jms.controller;

import com.payoneer.jms.model.error.ApiException;
import com.payoneer.jms.model.error.ErrorCode;
import com.payoneer.jms.model.error.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.stream.Collectors;

@ControllerAdvice
@ResponseBody
@Slf4j
public class ExceptionHandlerControllerAdvice {
    
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handleMismatchArgumentsExceptions(MethodArgumentTypeMismatchException exception) {
        return new ErrorResponse(
                ErrorCode.BAD_REQUEST.getCode(),
                String.format("Invalid value <%s> for parameter <%s>", exception.getValue(), exception.getName())
        );
    }
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handleNotValidArgumentsExceptions(MethodArgumentNotValidException exception) {
        
        StringBuilder invalidFields = new StringBuilder();
        invalidFields.append(exception.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(fieldError -> fieldError.getField() + ": " + fieldError.getDefaultMessage())
                .collect(Collectors.joining(", ")));
        
        String validationErrors = exception.getBindingResult()
                .getGlobalErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(", "));
        
        if (StringUtils.hasText(validationErrors)) {
            if (invalidFields.length() > 0) {
                invalidFields.append(", ");
            }
            invalidFields.append(validationErrors);
        }
        String message = String.format("Validation errors <%s>", invalidFields);
        return new ErrorResponse(ErrorCode.BAD_REQUEST.getCode(), message);
    }
    
    @ExceptionHandler(Exception.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ErrorResponse handleInternalErrors(Exception exception) {
        return new ErrorResponse(ErrorCode.INTERNAL_ERROR.getCode(), exception.getMessage());
    }
    
    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ErrorResponse> handleExternalServiceException(ApiException exception) {
        return ResponseEntity
                .status(exception.getHttpStatus())
                .body(new ErrorResponse(exception.getErrorCode(), exception.getErrorMessage()));
    }
    
}
