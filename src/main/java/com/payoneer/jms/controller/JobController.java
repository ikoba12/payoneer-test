package com.payoneer.jms.controller;

import com.payoneer.jms.model.bean.CreateJobRequest;
import com.payoneer.jms.model.bean.JobResponse;
import com.payoneer.jms.service.JobService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("jobs")
public class JobController {
    
    private final JobService jobService;
    
    public JobController(JobService jobService) {
        this.jobService = jobService;
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long create(@Valid @RequestBody CreateJobRequest request) {
        return jobService.create(request);
    }
    
    @PostMapping("/{id}/execute")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void executeJobById(@PathVariable Long id) {
        jobService.executeJobById(id);
    }
    
    @GetMapping
    public List<JobResponse> list() {
        return jobService.list();
    }
    
    @GetMapping("/{id}")
    public JobResponse one(@PathVariable Long id) {
        return jobService.one(id);
    }
}
