package com.payoneer.jms.model.entity;

public enum JobStatus {
    QUEUED, RUNNING, SUCCESS, FAILED
}
