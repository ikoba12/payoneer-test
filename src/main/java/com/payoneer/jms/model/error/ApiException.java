package com.payoneer.jms.model.error;

import org.springframework.http.HttpStatus;

public class ApiException extends RuntimeException {
    
    private final String errorCode;
    private final String errorMessage;
    private final HttpStatus httpStatus;
    
    public ApiException(ErrorCode errorCode, String errorMessage) {
        this(errorCode.getCode(), errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    public ApiException(String errorCode, String errorMessage, HttpStatus httpStatus) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.httpStatus = httpStatus;
    }
    
    @Override
    public String getMessage() {
        return this.errorMessage;
    }
    
    public String getErrorCode() {
        return errorCode;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }
    
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
    
}
