package com.payoneer.jms.model.error;

public enum ErrorCode {
    NOT_FOUND("not.found", "Entity not found"),
    BAD_REQUEST("bad.request", "Bad Request"),
    INTERNAL_ERROR("internal.error", "Internal server error"),
    JOB_EXECUTE_ERROR("job.execute.error", "Error while executing job");
    private final String code;
    
    private final String description;
    
    ErrorCode(String code, String description) {
        this.code = code;
        this.description = description;
    }
    
    public String getCode() {
        return code;
    }
    
    public String getDescription() {
        return description;
    }
    
}
