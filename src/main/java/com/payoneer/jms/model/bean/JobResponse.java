package com.payoneer.jms.model.bean;

import com.payoneer.jms.model.entity.JobStatus;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class JobResponse {
    private Long id;
    
    private String type;
    
    private String name;
    
    private LocalDateTime scheduledDate;
    
    private JobStatus jobStatus;
    
}
