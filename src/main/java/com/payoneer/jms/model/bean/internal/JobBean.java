package com.payoneer.jms.model.bean.internal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobBean {
    
    private String name;
    
    private String type;
    
}
