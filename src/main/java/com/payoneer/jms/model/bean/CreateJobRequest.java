package com.payoneer.jms.model.bean;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
public class CreateJobRequest {
    
    @NotBlank(message = "Type cannot be null")
    private String type;
    
    @NotBlank(message = "Name cannot be null")
    private String name;
    
    private LocalDateTime scheduledDate;
    
}
