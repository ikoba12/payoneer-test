package com.payoneer.jms.model.bean.internal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JobExecuteResultBean {
    private boolean success;
}
