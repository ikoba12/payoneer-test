package com.payoneer.jms.config;

import com.payoneer.jms.service.JobExecutorService;
import com.payoneer.jms.service.impl.DefaultJobExecutorServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfig {
    
    @Bean
    @ConditionalOnMissingBean(value = JobExecutorService.class)
    public JobExecutorService jobExecutorService() {
        return new DefaultJobExecutorServiceImpl();
    }
}
