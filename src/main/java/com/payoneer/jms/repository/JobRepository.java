package com.payoneer.jms.repository;

import com.payoneer.jms.model.entity.Job;
import com.payoneer.jms.model.entity.JobStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface JobRepository extends JpaRepository<Job, Long> {
    List<Job> findAllByScheduledDateIsLessThanEqualAndJobStatus(LocalDateTime scheduledDate, JobStatus jobStatus);
}
