package com.payoneer.jms.service;

import com.payoneer.jms.model.bean.internal.JobBean;
import com.payoneer.jms.model.bean.internal.JobExecuteResultBean;

import java.util.Optional;

public interface JobExecutorService {
    Optional<JobExecuteResultBean> executeJob(JobBean jobBean);
}
