package com.payoneer.jms.service.impl;

import com.payoneer.jms.model.bean.internal.JobBean;
import com.payoneer.jms.model.bean.internal.JobExecuteResultBean;
import com.payoneer.jms.service.JobExecutorService;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;


/*
 * This is a default job executioner which does not know how to execute any jobs, this bean should be overridden
 * Or here we can have logic to call an external api to execute the job and return the result.
 */
@Slf4j
public class DefaultJobExecutorServiceImpl implements JobExecutorService {
    @Override
    public Optional<JobExecuteResultBean> executeJob(JobBean jobBean) {
        log.info("Do not know how to execute any jobs");
        return Optional.empty();
    }
}
