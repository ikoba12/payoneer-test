package com.payoneer.jms.service.impl;

import com.payoneer.jms.model.bean.CreateJobRequest;
import com.payoneer.jms.model.bean.JobResponse;
import com.payoneer.jms.model.bean.internal.JobBean;
import com.payoneer.jms.model.bean.internal.JobExecuteResultBean;
import com.payoneer.jms.model.entity.Job;
import com.payoneer.jms.model.entity.JobStatus;
import com.payoneer.jms.model.error.ApiException;
import com.payoneer.jms.model.error.ErrorCode;
import com.payoneer.jms.repository.JobRepository;
import com.payoneer.jms.service.JobExecutorService;
import com.payoneer.jms.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class JobServiceImpl implements JobService {
    
    private final JobRepository jobRepository;
    
    private final JobExecutorService jobExecutorService;
    
    public JobServiceImpl(JobRepository jobRepository, JobExecutorService jobExecutorService) {
        this.jobRepository = jobRepository;
        this.jobExecutorService = jobExecutorService;
    }
    
    @Override
    public Long create(CreateJobRequest request) {
        log.info("creating job : {}", request);
        Job.JobBuilder builder = Job.builder()
                .name(request.getName())
                .type(request.getType())
                .jobStatus(JobStatus.QUEUED);
        LocalDateTime scheduledDate = request.getScheduledDate();
        if (scheduledDate == null) {
            scheduledDate = LocalDateTime.now();
        }
        builder.scheduledDate(scheduledDate);
        Job job = builder.build();
        job = jobRepository.save(job);
        return job.getId();
    }
    
    @Override
    public JobStatus executeJobById(Long id) {
        log.info("Executing job with id: {}", id);
        Job job = jobRepository
                .findById(id)
                .orElseThrow(() ->
                        new ApiException(ErrorCode.NOT_FOUND, String.format("Job not found for given Id : %s", id)
                        )
                );
        if (!job.getJobStatus().equals(JobStatus.QUEUED)) {
            throw new ApiException(ErrorCode.INTERNAL_ERROR, "Job is no longer in queued status");
        }
        updateJobStatus(job, JobStatus.RUNNING);
        JobStatus status;
        try {
            Optional<JobExecuteResultBean> result = jobExecutorService.executeJob(new JobBean(job.getName(), job.getType()));
            if (result.isPresent() && result.get().isSuccess()) {
                status = JobStatus.SUCCESS;
            } else {
                status = JobStatus.FAILED;
            }
        } catch (Exception e) {
            log.error("Job failed with exception", e);
            status = JobStatus.FAILED;
        }
        updateJobStatus(job, status);
        return status;
    }
    
    void updateJobStatus(Job job, JobStatus jobStatus) {
        log.info("updating job status. Job : {}, status : {}", job, jobStatus);
        job.setJobStatus(jobStatus);
        jobRepository.save(job);
    }
    
    @Override
    public List<JobResponse> list() {
        return jobRepository.findAll().stream().map(transformToJobResponse()).collect(Collectors.toList());
    }
    
    @Override
    public JobResponse one(Long id) {
        
        return transformToJobResponse()
                .apply(jobRepository
                        .findById(id)
                        .orElseThrow(() ->
                                new ApiException(ErrorCode.NOT_FOUND,
                                        String.format("Job not found for given Id : %s", id)
                                )
                        )
                );
    }
    
    @Override
    public List<JobResponse> listJobsToExecute() {
        return jobRepository.findAllByScheduledDateIsLessThanEqualAndJobStatus(LocalDateTime.now(), JobStatus.QUEUED)
                .stream()
                .map(transformToJobResponse())
                .collect(Collectors.toList());
    }
    
    private Function<Job, JobResponse> transformToJobResponse() {
        return job -> JobResponse
                .builder()
                .id(job.getId())
                .jobStatus(job.getJobStatus())
                .name(job.getName())
                .type(job.getType())
                .scheduledDate(job.getScheduledDate())
                .build();
    }
}
