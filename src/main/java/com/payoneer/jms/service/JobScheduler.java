package com.payoneer.jms.service;

import com.payoneer.jms.model.bean.JobResponse;
import com.payoneer.jms.model.entity.JobStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class JobScheduler {
    
    private final JobService jobService;
    
    public JobScheduler(JobService jobService) {
        this.jobService = jobService;
    }
    
    @Scheduled(fixedDelay = 1000)
    public void executeScheduledJobs() {
        List<JobResponse> jobsToExecute = jobService.listJobsToExecute();
        log.debug("Number of jobs to execute : {}", jobsToExecute.size());
        jobsToExecute.forEach(job -> {
            Long jobId = job.getId();
            JobStatus result = jobService.executeJobById(jobId);
            log.info("Executed job with Id : {}, result : {}", jobId, result);
        });
    }
    
}
