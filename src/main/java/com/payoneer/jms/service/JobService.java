package com.payoneer.jms.service;

import com.payoneer.jms.model.bean.CreateJobRequest;
import com.payoneer.jms.model.bean.JobResponse;
import com.payoneer.jms.model.entity.JobStatus;

import java.util.List;

public interface JobService {
    
    Long create(CreateJobRequest request);
    
    JobStatus executeJobById(Long id);
    
    List<JobResponse> list();
    
    JobResponse one(Long id);
    
    List<JobResponse> listJobsToExecute();
    
}
