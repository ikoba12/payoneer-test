package com.payoneer.controller

import com.payoneer.jms.JmsApplication
import com.payoneer.jms.model.entity.Job
import com.payoneer.jms.model.entity.JobStatus
import com.payoneer.jms.repository.JobRepository
import org.hamcrest.Matchers
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import javax.annotation.Resource
import java.time.LocalDateTime

import static groovy.json.JsonOutput.toJson
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@SpringBootTest(classes = [JmsApplication.class])
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class JobControllerTest extends Specification {

    @Resource
    JobRepository jobRepository

    @Resource
    private MockMvc mockMvc

    def 'when calling job list api then should returned apis stored in database'() {
        given:
        jobRepository.saveAll([
                new Job(1,
                        'testType',
                        'testName',
                        null,
                        JobStatus.QUEUED
                ),
                new Job(2,
                        'testType1',
                        'testName1',
                        null,
                        JobStatus.SUCCESS
                ),
                new Job(3,
                        'testType2',
                        'testName2',
                        null,
                        JobStatus.RUNNING
                ),
        ])
        expect:
        mockMvc.perform(get('/jobs'))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath('$', Matchers.hasSize(3)))
                .andExpect(jsonPath('$[0].id').value(1))
                .andExpect(jsonPath('$[0].type').value('testType'))
                .andExpect(jsonPath('$[0].jobStatus').value(JobStatus.QUEUED.toString()))
                .andExpect(jsonPath('$[0].name').value('testName'))
                .andExpect(jsonPath('$[1].id').value(2))
                .andExpect(jsonPath('$[1].type').value('testType1'))
                .andExpect(jsonPath('$[1].jobStatus').value(JobStatus.SUCCESS.toString()))
                .andExpect(jsonPath('$[1].name').value('testName1'))
                .andExpect(jsonPath('$[2].id').value(3))
                .andExpect(jsonPath('$[2].type').value('testType2'))
                .andExpect(jsonPath('$[2].jobStatus').value(JobStatus.RUNNING.toString()))
                .andExpect(jsonPath('$[2].name').value('testName2'))
    }

    def 'when calling job create api with valid data then should returned success'() {

        when:
        def call = mockMvc.perform(post("/jobs")
                .content(toJson(
                        [
                                type    : 'testType',
                                name    : 'testName',
                                schedule: null
                        ]
                ))
                .contentType(APPLICATION_JSON)
        )
        then:
        call
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath('$').value(1))
    }

    def 'when calling job create api with invalid data then should returned error'() {

        when:
        def call = mockMvc.perform(post("/jobs")
                .content(toJson(
                        [
                                type    : type,
                                name    : name,
                                schedule: LocalDateTime.now().toString()
                        ]
                ))
                .contentType(APPLICATION_JSON)
        )
        then:
        call
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath('$.code').value('bad.request'))
                .andExpect(jsonPath('$.description').value(errorDescription))
        where:
        name       | type       | errorDescription
        null       | 'testType' | 'Validation errors <name: Name cannot be null>'
        'testName' | null       | 'Validation errors <type: Type cannot be null>'
    }

    def 'when calling job execute api for valid job then should returned no content status'() {
        given:
        jobRepository.save(
                new Job(1,
                        'testType',
                        'testName',
                        null,
                        JobStatus.QUEUED
                )
        )
        when:
        def call = mockMvc.perform(post("/jobs/1/execute"))
        then:
        call
                .andExpect(status().isNoContent())
    }

    def 'when calling job execute api for already processed job then should returned error'() {
        given:
        jobRepository.save(
                new Job(1,
                        'testType',
                        'testName',
                        null,
                        JobStatus.FAILED
                )
        )
        when:
        def call = mockMvc.perform(post("/jobs/1/execute"))
        then:
        call
                .andExpect(status().is5xxServerError())
                .andExpect(jsonPath('$.code').value('internal.error'))
                .andExpect(jsonPath('$.description').value('Job is no longer in queued status'))

    }


}
